/* ********************* */
/*      Main Script      */
/* ********************* */

$(document).ready(function() {
	/* Scroll to TOP */
	$(".totop").hide();
	
	$(function(){
		$(window).scroll(function() {
			if ($(this).scrollTop() > 400) {
				$('.totop').fadeIn();
			} else {
				$('.totop').fadeOut();
			}
		});
		$('.totop a').click(function (e) {
			e.preventDefault();
			$('body,html').animate({scrollTop: 0}, 500);
		});
	});
	
	$(".main-nav li").hover(
		function (e) {
			$(this).find('ul').show();
		},
		function (e) {
			$(this).find('ul').hide();
		}
	);
	
});